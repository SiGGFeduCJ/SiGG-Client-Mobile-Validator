/* eslint-disable no-undef */   
{
      exports.fetchSomeData = () => {
            return new Promise(resolve => {
              setTimeout(() => resolve('success'), 100);
            });
          };
          
      exports.login = async () => {
            const response = await exports.fetchSomeData();
            return response === 'success';
      };
          
      
      exports.queryTxn = async () => {
            const response = await exports.fetchSomeData();
            return response === 'success';
      };
      
      
      exports.sign = async () => {
            const response = await exports.fetchSomeData();
            return response === 'success';
      };
      
      
      exports.submitValidatedTxn = async () => {
            const response = await exports.fetchSomeData();
            return response === 'success';
      };

}
   