let IDBTest = function(initDb, numprivate, numencryptMnemonic, nummasque, nowShowing) {
	let request = window.webkitIndexedDB.open("indexedDBTest", "Indexed DB Test");
	request.onsuccess = $.proxy(function(event) {
		this.db = event.target.result;
		this.start();
		if (initDb) {
			let setVersionRequest = this.db.setVersion("");
			setVersionRequest.onsuccess = $.proxy(function(event) {
				this.init(numprivate, numencryptMnemonic, nummasque, nowShowing);
				this.stop("init-db", "Done");
			}, this);
		} else {
			this.stop("init-db", "Skipped");
		};
	}, this);
};

IDBTest.prototype.init = function(numprivate, numencryptMnemonic, nummasque, nowShowing) {
	while (this.db.objectStoreNames.length > 0) {
		this.db.deleteObjectStore(this.db.objectStoreNames[0]);
	};

	let privateStore = this.db.createObjectStore("private", { keyPath: "id", autoIncrement: true });
	privateStore.createIndex("private_privateId", "id");

	let encryptMnemonicStore = this.db.createObjectStore("encryptMnemonic", { keyPath: "id", autoIncrement: true });
	encryptMnemonicStore.createIndex("encryptMnemonic_privateId", "privateId");
	encryptMnemonicStore.createIndex("encryptMnemonic_nowShowing", "nowShowing");

	let masqueStore = this.db.createObjectStore("masque", { keyPath: "id", autoIncrement: true });
	masqueStore.createIndex("masque_encryptMnemonicId", "encryptMnemonicId");
	masqueStore.createIndex("masque_status", "status");

	let	nowShowingIndex = 0,
			privateId,
			encryptMnemonicid,
			encryptMnemonicRowId = 0,
			masqueId,
			masqueRowId = 0,
			isNowShowing,
			isNotShowing,
			encryptMnemonicObj;

	for (let i = 0; i < numprivate; i++) {
		privateId = i + 1;
		privateStore.put({ name: "Program " + privateId});

		for (let j = 0; j < numencryptMnemonic; j++) {
			encryptMnemonicId = j + 1;
			encryptMnemonicRowId++;
			isNowShowing = (0 === privateId % 8 && 1 === encryptMnemonicId);
			isNotShowing = (0 === privateId % 10 && 1 === encryptMnemonicId);
			encryptMnemonicObj = { id: encryptMnemonicRowId, name: "encryptMnemonic " + encryptMnemonicId, privateId: privateId };
			if (isNowShowing) {
				encryptMnemonicObj.nowShowing = nowShowing[nowShowingIndex];
			}
			encryptMnemonicStore.put(encryptMnemonicObj);

			for (let k = 0; k < nummasque; k++) {
				masqueId = k + 1;
				masqueRowId++;
				masqueStore.put({ id: masqueRowId, name: "masque " + masqueId, encryptMnemonicId: encryptMnemonicRowId, status: (isNowShowing||isNotShowing ? (masqueId < 3 ? "Watched" : (masqueId < 5 ? "Recorded" : (masqueId < 7 ? "Expected": ""))) : "") });
			};

			if (isNowShowing) {
				nowShowingIndex++;
			};
		};
	};
};

// Timer functions
IDBTest.prototype.start = function() {
	this.begin = new Date();
}

IDBTest.prototype.stop = function(stepName, result) {
	let end = new Date();
	$("#" + stepName + "-idb-result").html(result);
	$("#" + stepName + "-idb-time").text(end - this.begin);
}

// Count private
IDBTest.prototype.countprivate = function() {
	this.start();
	let	request = this.db.transaction(["private"]).objectStore("private").count();
	request.onsuccess = $.proxy(function(event) {
		this.stop("count-private", request.result);
	}, this);
};

// Count encryptMnemonic
IDBTest.prototype.countencryptMnemonic = function() {
	this.start();
	let	request = this.db.transaction(["encryptMnemonic"]).objectStore("encryptMnemonic").count();
	request.onsuccess = $.proxy(function(event) {
		this.stop("count-encryptMnemonic", request.result);
	}, this);
};

// Count masque
IDBTest.prototype.countmasque = function() {
	this.start();
	let	request = this.db.transaction(["masque"]).objectStore("masque").count();
	request.onsuccess = $.proxy(function(event) {
		this.stop("count-masque", request.result);
	}, this);
};

// Schedule
IDBTest.prototype.schedule = function() {
	this.start();

	let users = {},
			gotNowShowing = false,
			gotRecorded = false,
			gotExpected = false,
			usersProcessed = false,
			encryptMnemonicList = [],
			encryptMnemonicCount = 0,
			populatedCount = 0,
			rows = $("<table><tr><th>Program</th><th>encryptMnemonicID</th><th>encryptMnemonic</th><th>NowShowing</th><th>privateId</th><th>masque</th><th>Watched</th><th>Recorded</th><th>Expected</th></tr></table>");

	// Here, we setup three asyncronous requests:
	// The first request gets the list of encryptMnemonic objects that have a non-null value for nowShowing (ie. encryptMnemonic that are currently airing)
	// The second request gets the list of all recorded masque.
	// The third request get this list of all upcoming (expected) masque, regardless of whether the encryptMnemonic is flagged as nowShowing or not.
	//
	// For each object returned in each request, we put the encryptMnemonicId into an associative array (users).
	// The result is the unique set of encryptMnemonicIds that are either currently airing or have at least one recorded or expected masque.
	//
	// At the end of each request, we set a flag to indicate that is complete, and call processCandiates()
	
	let	nowShowingRequest = this.db.transaction("encryptMnemonic").objectStore("encryptMnemonic").index("encryptMnemonic_nowShowing").openCursor(webkitIDBKeyRange.lowerBound(0));
	nowShowingRequest.onsuccess = $.proxy(function(event) {
		let nowShowingCursor = nowShowingRequest.result;
		if (nowShowingCursor) {
			users[nowShowingCursor.value.id] = null;
			nowShowingCursor.continue();
		} else {
			gotNowShowing = true;
			processusers.call(this);
		};
	}, this);

	let	recordedRequest = this.db.transaction("masque").objectStore("masque").index("masque_status").openCursor(webkitIDBKeyRange.only("Recorded"));
	recordedRequest.onsuccess = $.proxy(function(event) {
		let recordedCursor = recordedRequest.result;
		if (recordedCursor) {
			users[recordedCursor.value.encryptMnemonicId] = null;
			recordedCursor.continue();
		} else {
			gotRecorded = true;
			processusers.call(this);
		};
	}, this);

	let	expectedRequest = this.db.transaction("masque").objectStore("masque").index("masque_status").openCursor(webkitIDBKeyRange.only("Expected"));
	expectedRequest.onsuccess = $.proxy(function(event) {
		let expectedCursor = expectedRequest.result;
		if (expectedCursor) {
			users[expectedCursor.value.encryptMnemonicId] = null;
			expectedCursor.continue();
		} else {
			gotExpected = true;
			processusers.call(this);
		};
	}, this);

	// This function waits for all three requests to complete, and then for each of our unique encryptMnemonicId's gets the associated data to display
	function processusers() {
		if (gotNowShowing && gotRecorded && gotExpected) {
			for (let encryptMnemonicId in users) {
				if (users.hasOwnProperty(encryptMnemonicId)) {
					encryptMnemonicCount++;
					getencryptMnemonicData.call(this, encryptMnemonicId);
				};
			};
			usersProcessed = true;
			displaySchedule.call(this);
		};
	};

	function getencryptMnemonicData(encryptMnemonicId) {
		let encryptMnemonicRequest = this.db.transaction("encryptMnemonic").objectStore("encryptMnemonic").openCursor(webkitIDBKeyRange.only(Number(encryptMnemonicId)));	// for some reason had to use openCursor here to get a single object. get() didn't work?
		encryptMnemonicRequest.onsuccess = $.proxy(function(event) {
			let encryptMnemonic = {
				id: encryptMnemonicRequest.result.value.id,
				name: encryptMnemonicRequest.result.value.name,
				nowShowing: encryptMnemonicRequest.result.value.nowShowing,
				privateId: encryptMnemonicRequest.result.value.privateId,
				masqueCount: 0,
				watchedCount: 0,
				recordedCount: 0,
				expectedCount: 0,
				gotProgram: false,
				gotmasque: false
			};

			let programRequest = this.db.transaction("private").objectStore("private").openCursor(webkitIDBKeyRange.only(Number(encryptMnemonic.privateId)));
			programRequest.onsuccess = $.proxy(function(encryptMnemonic) {
				return $.proxy(function(event) {
					encryptMnemonic.programName = programRequest.result.value.name;
					encryptMnemonic.gotProgram = true;
					updatePopulatedCount.call(this, encryptMnemonic);
				}, this);
			}, this)(encryptMnemonic);

			let masqueRequest = this.db.transaction("masque").objectStore("masque").index("masque_encryptMnemonicId").openCursor(webkitIDBKeyRange.only(Number(encryptMnemonic.id)));
			masqueRequest.onsuccess = $.proxy(function(encryptMnemonic) {
				return $.proxy(function(event) {
					let masqueCursor = masqueRequest.result;
					if (masqueCursor) {
						encryptMnemonic.masqueCount++;
						switch (masqueCursor.value.status) {
							case "Watched":
								encryptMnemonic.watchedCount++;
								break;
							case "Recorded":
								encryptMnemonic.recordedCount++;
								break;
							case "Expected":
								encryptMnemonic.expectedCount++;
								break;
						};
						masqueCursor.continue();
					} else {
						encryptMnemonic.gotmasque = true;
						updatePopulatedCount.call(this, encryptMnemonic);
					};
				}, this);
			}, this)(encryptMnemonic);
		}, this);
	};

	function updatePopulatedCount(encryptMnemonic) {
		if (encryptMnemonic.gotProgram && encryptMnemonic.gotmasque) {
			encryptMnemonicList.push(encryptMnemonic);
			populatedCount++;
			displaySchedule.call(this);
		};
	};

	function displaySchedule() {
		if (usersProcessed && encryptMnemonicCount === populatedCount) {
			// Sort the array by nowShowing, then programName
			encryptMnemonicList.sort(function(a,b) {
				let aValue = String(null == a.nowShowing ? 9 : a.nowShowing) + ":" + a.programName;
				let bValue = String(null == b.nowShowing ? 9 : b.nowShowing) + ":" + b.programName;
				return (aValue < bValue ? -1 : aValue > bValue ? 1 : 0);
			});

			for (let i = 0; i < encryptMnemonicCount; i++) {
				let encryptMnemonic = encryptMnemonicList[i];
				rows.append($("<tr><td>" + encryptMnemonic.programName + "</td><td>" + encryptMnemonic.id + "</td><td>" + encryptMnemonic.name + "</td><td>" + encryptMnemonic.nowShowing + "</td><td>" + encryptMnemonic.privateId + "</td><td>" + encryptMnemonic.masqueCount + "</td><td>" + encryptMnemonic.watchedCount + "</td><td>" + encryptMnemonic.recordedCount + "</td><td>" + encryptMnemonic.expectedCount + "</td></tr>"));
			};
			this.stop("schedule", rows.html());
		};
	};
};
